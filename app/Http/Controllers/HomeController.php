<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function welcome(Request $request){
        $depan = $request->Awal;
        $belakang = $request->Akhir;

        return view('data.welcome', compact('depan','belakang'));
    }
}
